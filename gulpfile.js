const { parallel, series } = require("gulp");
const { scripts } = require("./gulp-tasks/scripts.js");
const { styles } = require("./gulp-tasks/styles.js");
const { images, images_copy } = require("./gulp-tasks/images.js");
const { serv } = require("./gulp-tasks/serv.js");
const { clear } = require("./gulp-tasks/clear.js");
const { watcher } = require("./gulp-tasks/watcher.js");

exports.dev = parallel(serv, watcher, series(images_copy, styles, scripts));
exports.build = series(series(clear,images, styles, scripts));
